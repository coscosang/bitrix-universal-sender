<?

use \Bitrix\Main\Application;

use \Bitrix\Main\Context;

use \Bitrix\Main\Loader;

Bitrix\Main\Loader::includeModule('sale');
Bitrix\Main\Loader::includeModule('catalog');


$siteId = Context::getCurrent()->getSite();
function getPropertyByCode($propertyCollection, $code)  {
    foreach ($propertyCollection as $property)
    {
        if($property->getField('CODE') == $code)
            return $property;
    }
}

$products = array();


$arElement = \CIBlockElement::GetList([], ['ID' => $arPost['ID']], false, false, ['NAME', 'PROPERTY_PRICE', 'DETAIL_PAGE_URL'])->GetNext();


//print_r($arPost['PROSUCTS']);
//print_r($products);
$basket = \Bitrix\Sale\Basket::create($siteId);

$item = $basket->createItem("catalog", $arElement['ID']);

$arPrice = CPrice::GetBasePrice($arElement['ID']);
//print_r($arPrice);
$item->setFields(array(
 'NAME' => $arElement['NAME'],
 'PRICE' => $arPrice['PRICE'],
 'CURRENCY' => $arPrice['CURRENCY'],
 'QUANTITY' => 1,
 'PRODUCT_PROVIDER_CLASS' => '\Bitrix\Catalog\Product\CatalogProvider'
));

if(isset($arPost['COMPLECT_ID'])) {
    $basketPropertyCollection = $item->getPropertyCollection();
    $basketPropertyCollection->setProperty(array('COMPLECT' => array(
       'NAME' => 'Комплект',
       'CODE' => 'COMPLECT',
       'VALUE' => "ID: {$arPost['COMPLECT_ID']}; Имя: {$arPost['COMPLECT_NAME']}; Цена:  {$arPost['COMPLECT_PRICE']}",
       'SORT' => 100
    )));
}

if($USER->IsAuthorized()){
	$USER_ID = $USER->GetID();
}else{
	$USER_ID = 1;
}
$currency = \Bitrix\Currency\CurrencyManager::getBaseCurrency();
///////////////////////////////////////////////////////

$order = Bitrix\Sale\Order::create($siteId, $USER_ID, $currency);

$order->setPersonTypeId(1);
$order->setBasket($basket);
//$order->setField("STATUS_ID","DM");

$shipmentCollection = $order->getShipmentCollection();
$shipment = $shipmentCollection->createItem(
    Bitrix\Sale\Delivery\Services\Manager::getObjectById(36)
);

$shipmentItemCollection = $shipment->getShipmentItemCollection();

foreach ($basket as $basketItem)
{
    $item = $shipmentItemCollection->createItem($basketItem);
    $item->setQuantity($basketItem->getQuantity());
}

//////////////////////////////////////////////////////
$fileID = false;
foreach ($_FILES as $key => $arFile) {
    if($key == 'FILE' && $arFile['size'] > 0){
        $file_to_save = array_merge($arFile, array("del" => null, "MODULE_ID" => "sale"));
        $fileID = CFile::SaveFile($file_to_save, "sale");
        continue;
    }
    if($arFile['size'] > 0){
        $file_to_save = array_merge($arFile, array("del" => null, "MODULE_ID" => "sale"));
        $files_ids[] = CFile::SaveFile($file_to_save, "sale");
    }
}
//////////////////////////////////////////////////////


$propertyCollection = $order->getPropertyCollection();
$propertyCodeToId = array();
foreach($propertyCollection as $propertyValue)
    $propertyCodeToId[$propertyValue->getField('CODE')] = $propertyValue->getField('ORDER_PROPS_ID');

$propertyValue=$propertyCollection->getItemByOrderPropertyId($propertyCodeToId['NAME']);
$propertyValue->setValue($arPost['NAME']);

$propertyValue=$propertyCollection->getItemByOrderPropertyId($propertyCodeToId['PHONE']);
$propertyValue->setValue($arPost['PHONE']);



if($fileID){
	$propertyValue=$propertyCollection->getItemByOrderPropertyId($propertyCodeToId['FILE']);
	$propertyValue->setValue($fileID);
}

if(count($files_ids) > 0){
	$propertyValue=$propertyCollection->getItemByOrderPropertyId($propertyCodeToId['FILES']);
	$propertyValue->setValue($files_ids);
}


if ($arPost['TEXT']) {
    $order->setField('USER_DESCRIPTION', $arPost['TEXT']); // Устанавливаем поля комментария покупателя
}


/** @var Sale\BasketItem $basketItem */


$paymentCollection = $order->getPaymentCollection();
$payment = $paymentCollection->createItem(
    Bitrix\Sale\PaySystem\Manager::getObjectById(11)
);
$payment->setField("SUM", $order->getPrice());
$payment->setField("CURRENCY", $order->getCurrency());



try{
	$order->doFinalAction(true);
	$result = $order->save();

	$orderId = $arPost['ORDER_ID'] = $order->getId();
	$accountNumber = $order->getField('ACCOUNT_NUMBER'); // генерируемый номер заказа

	if (!$result->isSuccess()){
	    $json["ERROR"] = $result->getErrors();
	}else{
	    //CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());
	}
}catch(Exeption $e){
	$json["ERROR"] = $e;
}

unset($arPost['PROSUCTS']);
if(!CEvent::Send('CART_BUY', 's2', $arPost, "N", "", $files_ids, 'ru')){
	$json["ERROR"] = 'Заявка не отправлена по техническим причинам! Вы можете позвонить нам или написав на электронную почту';
}else{
	$json["SUCCESS"] = "Заявка успешно отправлена, мы свяжемся с Вами в ближайшее время";
}
die(json_encode($json));
