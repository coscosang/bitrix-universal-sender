<?
define('PUBLIC_AJAX_MODE', true);
define('NO_KEEP_STATISTIC', 'Y');
define('NO_AGENT_STATISTIC', 'Y');
define('NOT_CHECK_PERMISSIONS', true);
define('BX_NO_ACCELERATOR_RESET', true);
define('IBLOCK_EVENTS', 42);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$files_ids = array();
if($arPost = $_REQUEST["SEND"]){
	if($arPost["STR"] != $_COOKIE['PHPSESSID'] . $_COOKIE['STR_FIELD'] or $_COOKIE['STR_FIELD'] != date('d')){
		//$json["ERROR"] = 'Извините, мы заметили подозрительную активность. Пожалуйста, обновите страницу и попробойте снорва.';
		//die(json_encode($json));
	}
	$event = isset($arPost['EVENT_NAME'])?$arPost['EVENT_NAME']:'CALL_BACK';
	if($arPost["CPTH"] == '5MKVG6B76MREWT9G8SD5'){
	    // Так можно подрубать отдельные обработчики в зависимости от типа события
		if($event == 'ONE_CLICK_PRODUCT'){
			//print_r($arPost);
			include './order.php';
		}
        // Save infoblock
        if(CModule::IncludeModule("iblock")){
            $message = '';
            foreach ($_REQUEST["SEND"] as $key => $item) {
                if($key != 'CPTH' && $key != 'STR' && $key != 'THEME')
                    $message .= $key . ': ' . $item . PHP_EOL;
            }
            $arFields = array(
                "ACTIVE" => "Y",
                "IBLOCK_ID" => IBLOCK_EVENTS,
                "NAME" => $_REQUEST["SEND"]['THEME'],
                "DETAIL_TEXT" => $message,
            );
            $oElement = new CIBlockElement();
            $idElement = $oElement->Add($arFields, false, false, true);
        }
		//////////////////////////////////////////////////////
		$fileID = false;
		foreach ($_FILES as $key => $arFile) {
		    if($arFile['size'] > 0){
		        $file_to_save = array_merge($arFile, array("del" => null, "MODULE_ID" => "mail")); 
		        $files_ids[] = CFile::SaveFile($file_to_save, "mail"); 
		    }
		}
		if(count($files_ids) > 0){
			$isSend = CEvent::Send($event, SITE_ID, $arPost, "N", "", $files_ids);
		}else{
			$isSend = CEvent::SendImmediate($event, SITE_ID, $arPost, "N", "");
		}
		if(!$isSend){
			$json["ERROR"] = 'Заявка не отправлена по техническим причинам! Вы можете позвонить нам или написав на электронную почту';
		}else{
			$json["SUCCESS"] = "Заявка успешно отправлена, мы свяжемся с Вами в ближайшее время";
		}
	}else{
		$json["ERROR"] = 'Текст с картинки введен неверно!';
	}
}else{
	$json["ERROR"] = 'Заполните поля';
}
die(json_encode($json));
