function goToEvent(eventName, arParam){
    console.log('EVENT_NAME: "' + eventName + '"');
    try{
        if(eventName == 'Lead' || eventName == 'PageView'){
            fbq('track', eventName);
        }else{
            fbq('trackCustom', eventName, arParam);
        }

    }catch(e){
        console.log('EVENT_FACEBOOK_ERROR: "' + eventName + '"');
    }
    try{
        roistat.event.send(eventName);
    }catch(e){
        console.log('EVENT_ROISTAT_ERROR:"' + eventName + '"');
    }
    try{
        ga('send', 'event', 'leadform', eventName);
    }catch(e){
        console.log('EVENT_GOOGLE_ERROR:"' + eventName + '"');
    }
    try{
        // Указать номер метрики!
        ym(11111111, 'reachGoal', eventName);
        // OLD
        //yaCounter47948408.reachGoal(eventName);
    }catch(e){
        console.log('EVENT_YANDEX_ERROR: "' + eventName + '"');
    }
}

var ajaxFormBootstrap = {
    initialize : function () {
        this.setUpListeners();
    },
    setUpListeners: function () {
        $('[data-send="BTN"]').on('click', ajaxFormBootstrap.submitForm);
        $('[data-send="FORM"]').on('keydown', '.has-error', ajaxFormBootstrap.removeError);
    },
    submitForm: function (e) {
        //e.preventDefault();
        console.log('START_SEND');
        $('.fieldS').val('5MKVG6B76MREWT9G8SD5');
        var form = $(this).closest('[data-send="FORM"]'),
            submitBtn = form.find('[data-send="BTN"]');
        // если валидация не проходит - то дальше не идём
        if ( ajaxFormBootstrap.validateForm(form) === false ) return false;

        // против повторного нажатия
        submitBtn.attr({disabled: 'disabled'});
        form.find('.result').html('<div class="alert alert-info">Отправка, подождите...</div>');
        var eventName = form.find('input[name="SEND[EVENT_NAME]"]').val();
        console.log('EVENT_NAME: ' + eventName);

        var formID = form.attr('id');
        var forma = document.forms.namedItem(formID);
        var formData = new FormData(forma);
        console.log('SEND_DATA: ');
        console.log(formData);

        var xhr = new XMLHttpRequest();
        var url = $(form).attr('data-action');
        xhr.open("POST", $(form).attr('data-action'));

        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                if(xhr.status == 200) {
                    respond = xhr.responseText;
                    var arRespond = JSON.parse(respond);
                    console.log(arRespond);
                    if(arRespond.ERROR) {
                        result = '<div class="alert alert-danger">' + arRespond.ERROR + '</div>';
                        form.find('.result').html(result);
                    } else {
                        result = '<div class="alert alert-success">' + arRespond.SUCCESS + '</div>';
                        
                        if(form.attr('data-send-hide') == "Y"){
                            form.html(result);
                        }else{
                            form.find('.result').html(result);
                            form.trigger('reset');
                        }
                        try{
                            goToEvent(eventName);
                        }catch (e) {
                            console.log('Подключите функцию goToEvent(), для отправки целей.')
                        }

                        $('.fieldS').val('HJKFHSD7F6DSF7SDF98');
                        form.trigger('reset');
                    }
                }else{
                    result = `<div class="alert alert-danger">Ошибка отправки, проверьте наличие файла: ${url}</div>`;
                    form.find('.result').html(result);
                }
            }
        };

        xhr.send(formData);
        submitBtn.removeAttr("disabled");

    },

    validateForm: function (form){
        var inputs = form.find('.required'),
            valid = true;
        //inputs.tooltip('destroy');
        $.each(inputs, function(index, val) {
            var input = $(val),
                val = input.val(),
                formGrout = input,
                textError = input.attr("data-error"),
                type = input.attr("type");
            if(type == "checkbox"){
                console.log('CHECKED_CHECK');
                if(!input.is(':checked')){
                    formGrout.addClass('has-error').removeClass('has-success');
                    valid = false;
                }
                //return;
            }else if(val.length === 0){
                formGrout.addClass('has-error').removeClass('has-success');
                valid = false;
            }else{

                if(input.attr("type") == "email"){
                    formGrout.addClass('has-error').removeClass('has-success');
                    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
                    if (!pattern.test(val)) {
                        valid = false;
                    }else{
                        formGrout.removeClass('has-error').addClass('has-success');
                        //input.tooltip('hide');
                    }
                }else{
                    formGrout.removeClass('has-error').addClass('has-success');
                    //input.tooltip('hide');
                }
            }
        });
        return valid;
    },
    removeError: function() {
        $(this).removeClass('has-error');
    }
}
$(function() {
    ajaxFormBootstrap.initialize();
});
